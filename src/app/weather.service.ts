import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { HttpClient } from '@angular/common/http';



@Injectable()
export class WeatherService {

  public loc: any = { latitud: null, longitud: null };
  private loc$ = new Subject<any>();  
  private dateNow$ = new Subject<any>(); 

  constructor(public http: HttpClient) {}  

  getLocation() {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(this.showPosition.bind(this));
      } else {
        return this.loc;
      }
  }

  showPosition(position) { // callBack
      this.loc.latitud = position.coords.latitude;
      this.loc.longitud = position.coords.longitude;
      this.loc$.next(this.loc);
      this.loc$.complete();
      //return this.loc;
  }

  dateNow (){
    setInterval(() => {
      this.dateNow$.next(Date.now())
    }, 1000);
  }
    
  

  getLoc$(): Observable<any> {
    return this.loc$.asObservable();
  }

  getDateNow$(): Observable<any> {
    return this.dateNow$.asObservable();
  }
  
  
  getData (loc):Observable<any> {
    var url = "https://api.apixu.com/v1/forecast.json?key=35755790de2e4e31823160532182011&q=" 
                  + loc.latitud + "," + loc.longitud + "&days=6";    
    return this.http.get<any>(url);
  }

  getDataByCity (city):Observable<any> {
    var url = "https://api.apixu.com/v1/forecast.json?key=35755790de2e4e31823160532182011&q=" + city + "&days=6";    
    return this.http.get<any>(url);
  }
  


}
