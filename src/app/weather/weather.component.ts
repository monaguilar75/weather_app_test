import { Component, OnInit } from '@angular/core';

import { WeatherService } from '../weather.service';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html'
})



export class WeatherComponent implements OnInit {
  
  public dataJson: any = null;
  public dataJsonS: any = null;

  public loc: any = { latitud: null, longitud: null }; 
  public locSubscription: Subscription;
  public loc$: Observable<any>;  

  public dateNow:Date;
  public dateNowSubscription: Subscription;
  public dateNow$: Observable<any>;   
  
  public hoy:number = Date.now() // nos devuelve un epoch
  public process:string = "loading" // loading | form | dataOk

  public cityForm: FormGroup;
  
  constructor(public weatherService:WeatherService) {

    this.cityForm = new FormGroup({
      'city': new FormControl('', Validators.required)
    });
  }  

  get city() { return this.cityForm.get('city').value };

  ngOnInit(): void {
    
    this.weatherService.getLocation();
    this.loc$ = this.weatherService.getLoc$();
    this.locSubscription = this.loc$.subscribe(
      res => {
        this.loc = res;
        //console.log(res);
      },
      e => console.log(e),
      () => this.getData()
    );      
    
    // timer
    this.weatherService.dateNow();
    this.dateNow$ = this.weatherService.getDateNow$();
    this.dateNowSubscription = this.dateNow$.subscribe(
      res => {
        this.dateNow = res;
        //console.log(this.dateNow);
      }
    )
  }

  getData () {

    this.weatherService.getData(this.loc)
      .subscribe (
        (res) => {
          this.dataJson = res;
          //console.log(this.dataJson);
          this.process = "dataOk"; // loading | dataOk
        }
      )
    
    
    // JSON para no hacer peticiones a la API, que están limitadas a 60/hora
    /*
    this.dataJson = '{"location":{"name":"El Clot","region":"Catalonia","country":"Spain","lat":41.43,"lon":2.17,"tz_id":"Europe/Madrid","localtime_epoch":1542789873,"localtime":"2018-11-21 9:44"},"current":{"last_updated_epoch":1542789010,"last_updated":"2018-11-21 09:30","temp_c":10.0,"temp_f":50.0,"is_day":1,"condition":{"text":"Sunny","icon":"//cdn.apixu.com/weather/64x64/day/113.png","code":1000},"wind_mph":0.0,"wind_kph":0.0,"wind_degree":280,"wind_dir":"W","pressure_mb":1009.0,"pressure_in":30.3,"precip_mm":0.0,"precip_in":0.0,"humidity":87,"cloud":0,"feelslike_c":10.0,"feelslike_f":50.0,"vis_km":10.0,"vis_miles":6.0,"uv":4.0},"forecast":{"forecastday":[{"date":"2018-11-21","date_epoch":1542758400,"day":{"maxtemp_c":17.1,"maxtemp_f":62.8,"mintemp_c":13.0,"mintemp_f":55.4,"avgtemp_c":14.5,"avgtemp_f":58.0,"maxwind_mph":11.9,"maxwind_kph":19.1,"totalprecip_mm":1.2,"totalprecip_in":0.05,"avgvis_km":18.5,"avgvis_miles":11.0,"avghumidity":62.0,"condition":{"text":"Heavy rain at times","icon":"//cdn.apixu.com/weather/64x64/day/305.png","code":1192},"uv":1.8},"astro":{"sunrise":"07:47 AM","sunset":"05:27 PM","moonrise":"04:47 PM","moonset":"05:26 AM"}},{"date":"2018-11-22","date_epoch":1542844800,"day":{"maxtemp_c":16.5,"maxtemp_f":61.7,"mintemp_c":14.1,"mintemp_f":57.4,"avgtemp_c":14.5,"avgtemp_f":58.0,"maxwind_mph":5.6,"maxwind_kph":9.0,"totalprecip_mm":0.0,"totalprecip_in":0.0,"avgvis_km":19.6,"avgvis_miles":12.0,"avghumidity":52.0,"condition":{"text":"Partly cloudy","icon":"//cdn.apixu.com/weather/64x64/day/116.png","code":1003},"uv":1.8},"astro":{"sunrise":"07:48 AM","sunset":"05:27 PM","moonrise":"05:22 PM","moonset":"06:33 AM"}},{"date":"2018-11-23","date_epoch":1542931200,"day":{"maxtemp_c":16.1,"maxtemp_f":61.0,"mintemp_c":12.6,"mintemp_f":54.7,"avgtemp_c":14.9,"avgtemp_f":58.9,"maxwind_mph":14.1,"maxwind_kph":22.7,"totalprecip_mm":1.2,"totalprecip_in":0.05,"avgvis_km":17.5,"avgvis_miles":10.0,"avghumidity":66.0,"condition":{"text":"Heavy rain at times","icon":"//cdn.apixu.com/weather/64x64/day/305.png","code":1192},"uv":1.5},"astro":{"sunrise":"07:49 AM","sunset":"05:26 PM","moonrise":"06:02 PM","moonset":"07:43 AM"}},{"date":"2018-11-24","date_epoch":1543017600,"day":{"maxtemp_c":15.2,"maxtemp_f":59.4,"mintemp_c":11.9,"mintemp_f":53.4,"avgtemp_c":13.3,"avgtemp_f":55.9,"maxwind_mph":11.6,"maxwind_kph":18.7,"totalprecip_mm":1.1,"totalprecip_in":0.04,"avgvis_km":20.0,"avgvis_miles":12.0,"avghumidity":55.0,"condition":{"text":"Heavy rain at times","icon":"//cdn.apixu.com/weather/64x64/day/305.png","code":1192},"uv":1.8},"astro":{"sunrise":"07:50 AM","sunset":"05:26 PM","moonrise":"06:49 PM","moonset":"08:52 AM"}},{"date":"2018-11-25","date_epoch":1543104000,"day":{"maxtemp_c":15.1,"maxtemp_f":59.2,"mintemp_c":13.1,"mintemp_f":55.6,"avgtemp_c":13.1,"avgtemp_f":55.6,"maxwind_mph":11.6,"maxwind_kph":18.7,"totalprecip_mm":0.0,"totalprecip_in":0.0,"avgvis_km":19.4,"avgvis_miles":12.0,"avghumidity":52.0,"condition":{"text":"Partly cloudy","icon":"//cdn.apixu.com/weather/64x64/day/116.png","code":1003},"uv":1.8},"astro":{"sunrise":"07:52 AM","sunset":"05:25 PM","moonrise":"07:44 PM","moonset":"09:59 AM"}},{"date":"2018-11-26","date_epoch":1543190400,"day":{"maxtemp_c":16.3,"maxtemp_f":61.3,"mintemp_c":12.1,"mintemp_f":53.8,"avgtemp_c":13.9,"avgtemp_f":57.0,"maxwind_mph":15.2,"maxwind_kph":24.5,"totalprecip_mm":0.0,"totalprecip_in":0.0,"avgvis_km":19.5,"avgvis_miles":12.0,"avghumidity":61.0,"condition":{"text":"Partly cloudy","icon":"//cdn.apixu.com/weather/64x64/day/116.png","code":1003},"uv":39960.0},"astro":{"sunrise":"07:53 AM","sunset":"05:25 PM","moonrise":"08:46 PM","moonset":"11:01 AM"}}]}}';
    this.dataJson = JSON.parse(this.dataJson);
    //console.log(this.dataJson);

    this.process = "dataOk"; // loading | form | dataOk
    */
  }

  getDataByCity() {
    this.process = "loading";
    this.weatherService.getDataByCity(this.city)
      .subscribe (
        (res) => {
          this.dataJson = res;
          //console.log(this.dataJson);
          this.process = "dataOk"; // loading | form | dataOk
        }
      )
  }

  showForm() {
    this.process = "form";
  }




  ngOnDestroy() {
    this.locSubscription.unsubscribe();
    this.dateNowSubscription.unsubscribe();
  }

  
  
  
}
